require('dotenv').config()

const pjson = require('../package.json'),
    fs = require('fs'),
    express = require('express'),
    cookieParser = require('cookie-parser'),
    path = require('path'),
    user_routes = require('./api/routes/user_routes'),
    account_routes = require('./api/routes/account_routes'),
    card_routes = require('./api/routes/card_routes'),
    movement_routes = require('./api/routes/movement_routes'),
    transfer_routes = require('./api/routes/transfer_routes'),
    app = express(),
    port = process.env.PORT || 5000,
    port_ssl = 5443,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    args = require('minimist')(process.argv.slice(2)),
    {
        logger,
        environment
    } = require('techubank-commons'),
    mlab_username = process.env.MLAB_USERNAME || environment.value.techubank_mlab_username,
    mlab_password = process.env.MLAB_PASSWORD || environment.value.techubank_mlab_password,
    mlab_host = process.env.MLAB_HOST || environment.value.techubank_mlab_host,
    mlab_databasename = process.env.MLAB_DATABASENAME || environment.value.techubank_mlab_databasename

if (args.logs) {
    logger.info(`Redirecting logs into ${args.logs}`)
    const access = fs.createWriteStream(args.logs)
    process.stdout.write = process.stderr.write = access.write.bind(access)
}

_connect_mongo()

app.use(cookieParser())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())

app.use('/ws-doc', express.static(path.join(__dirname, '..', 'ws-doc')))
app.use('/v1/user', user_routes)
app.use('/v1/account', account_routes)
app.use('/v1/card', card_routes)
app.use('/v1/movement', movement_routes)
app.use('/v1/transfer', transfer_routes)

// CHECK IF COMMUNICATION IS OVER HTTP OR HTTPS
//if (environment.value.protocol === 'https') {
const https = require('https'),
    helmet = require('helmet')

app.use(helmet()) // Add Helmet as a middleware
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0 // this allows self-signed cer
https.createServer({
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('certificate.pem')
}, app).listen(port_ssl)
//} else {
app.listen(port)
//}

logger.info(`***** techubank-mlab@${pjson.version} *************`)
logger.info(`App listening HTTP on port ${port}`)
logger.info(`App listening HTTPS on port ${port_ssl}`)


// Add headers
app.use(function (req, res, next) {
    res.status(404).send({
        url: req.originalUrl + ' not found'
    })

    next()
})


function _connect_mongo() {
    mongoose.Promise = global.Promise
    mongoose.connect(`mongodb://${mlab_username}:${mlab_password}@${mlab_host}/${mlab_databasename}`, {
        useNewUrlParser: true,
        autoIndex: true,
        useCreateIndex: true,
        useFindAndModify: false
    })
}