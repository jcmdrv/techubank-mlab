/**
 * @fileoverview
 * Provides the Techubank user (CRUD) features.
 * 
 */

const {
        user
    } = require('../models/user'),
    {
        mongooseUtils,
        crypt,
        logger
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class'),
    moment = require('moment')


module.exports = class UserController extends BaseController {
    constructor() {
        super()
    }

    static getUser(req, res) {
        const user_controller = new UserController()

        return user_controller._getUser(req, res)
    }

    static findUsers(req, res) {
        const user_controller = new UserController()

        return user_controller._findUsers(req, res)
    }

    static updateUser(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._updateUser(req, res)
        })
    }

    static resetPassword(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._resetPassword(req, res)
        })        
    }

    static changePassword(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._changePassword(req, res)
        })          
    }

    static createUser(req, res) {
        const user_controller = new UserController()

        return user_controller._createUser(req, res)
    }

    static userExists(temp) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._userExists(temp)
        })          
    }

    /**
* Function to consult a Techubank user by it's username
* @param  {Object} req - Request info
* @param  {Object} res - Response info
*/
    _getUser(req, res) {
        const query = user.findOne({
            username: req.params['username']
        })

        return query.exec().then((user_found) => {
            res.json(user_found)
        }).catch((error) => {
            logger.info(`Error looking up the user: ${error}`)
            res.statusCode = 400
            res.json({
                internal_code: 'error-looking-up-user',
                message: 'Error looking up the user. Check input values'
            })
        })
    }

    /**
	* Function to consult Techubank users
	* @param  {Object} req - Request info
	* @param  {Object} res - Response info
	*/
    _findUsers(req, res) {
        if (req.body.user_info) {
            const query = user.find(req.body.user_info)

            return query.exec().then((user_found) => {
                res.json(user_found)
            }).catch((error) => {
                logger.info(`Error looking up the users: ${error}`)
                res.statusCode = 400
                res.json({
                    internal_code: 'error-looking-up-users',
                    message: 'Error looking up the users. Check input values'
                })
            })
        }
    }

    /**
	 * Function to update a Techubank user from it's username
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _updateUser(req, res) {      
        logger.info(`Updating user ${req.body.username} info`)
        return this.client_get(`/v1/user/${req.body.username}/info`).then((res_mlab) => {
            const user_found = JSON.parse(res_mlab.body)
            
            // check if password must be changed
            if (user_found.password != req.body.password) {
                logger.info('User is going to change current password')
                req.body.password = crypt.hash(req.body.password) 
            } else {
                logger.info('Password DID NOT CHANGE')
            }
             
            // format date
            req.body.birth_date = moment(req.body.birth_date, 'DD/MM/YYYY')

            return mongooseUtils.findOneAndUpdate(user, {
                username: req.body.username
            }, req.body)
                .then(() => {
                    logger.info('saved!!!!')
                    res.json(req.body)
                })
                .catch((error) => {
                    logger.info(`Error updating the user: ${error}`)
                    res.statusCode = 400
                    return res.json({
                        internal_code: 'error-updating-user',
                        message: 'Error updating the user. Check input values'
                    })
                })
        }).catch((error) => {
            logger.info(`Error searching the user: ${error}`)
            res.statusCode = 400
            return res.json({
                internal_code: 'error-updating-user',
                message: 'Error updating the user. User not found'
            })
        })
    }

    _resetPassword(req, res) {
        logger.info(`Reset password to ${req.body.username}`)

        return this.client_get(`/v1/user/info/${req.body.username}`).then((res_mlab) => {
            const user_found = JSON.parse(res_mlab.body)
            user_found.password = crypt.hash(req.body.password)

            return mongooseUtils.update(user, {
                username: req.body.username
            }, user_found)
                .then(() => {
                    logger.info('Password updated!!!!')
                    res.json(user_found)
                })
                .catch((error) => {
                    logger.info(`Error reseting the password: ${error}`)
                    res.statusCode = 400
                    return res.json({
                        internal_code: 'error-reseting-password',
                        message: 'Error reseting the password. Check input values'
                    })
                })
        }).catch((error) => {
            logger.info(`Error reseting the password: ${error}`)
            res.statusCode = 400
            return res.json({
                internal_code: 'error-reseting-password',
                message: 'Error reseting the password. Check input values'
            })		
        })
    }


    _changePassword(req, res) {
        return this.client_get(`/v1/user/info/${req.body.username}`).then((res_mlab) => {
            const user_found = JSON.parse(res_mlab.body)

            req.body.new_password = crypt.hash(req.body.new_password)

            if (!crypt.checkpassword(req.body.current_password, user_found.password)) {
                res.statusCode = 400
                res.send('Error reseting the password')
            } else {

                user_found.password = req.body.new_password

                return mongooseUtils.update(user, {
                    username: req.body.username
                }, user_found)
                    .then(() => {
                        logger.info('Password updated!!!!')
                        res.json(user_found)
                    })
                    .catch((error) => {
                        logger.info(`Error updating the password: ${error}`)
                        res.statusCode = 400
                        return res.json({
                            internal_code: 'error-updating-password',
                            message: 'Error updating the password. Check input values'
                        })
                    })
            }
        }).catch((error) => {
            logger.error(`_changePassword: ${error}`)
            res.statusCode = 400
            return res.body(error)
        })
    }

    _createUser(req, res) {
        const new_user = new user()

        logger.info(`Saving new User ${req.body.username}`)

        Object.assign(new_user, req.body)
        new_user.birth_date = moment(req.body.birth_date, 'DD/MM/YYYY')
        new_user.register_date = new Date()
        new_user.status = 'ACTIVE'
        new_user.password = crypt.hash(new_user.password)

        return mongooseUtils.save(new_user)
            .then(() => {
                res.json(new_user)
            })
            .catch((error) => {
                logger.info(`Error saving new User: ${error}`)
                res.statusCode = 400
                res.json({
                    internal_code: 'error-saving-user',
                    message: 'Error saving new User. Check input values'
                })
            })
    }

    /**
	 * Function to consult if a username, email or a nif have alreday used in Techubank
	 * @param  {User} temp - user
	 * 
	 */
    async _userExists(temp) {
        logger.info(`Looking up the user fields ${temp.username}, ${temp.email} and ${temp.nif}`)

        const query = { $or: [{ username: temp.username }, { email: temp.email }, { nif: temp.nif }] }

        return new Promise((resolve, reject) => {
            return mongooseUtils.findOne(user, query)
                .then((document_found) => {
                    logger.info('Any users fields already in use')
                    resolve(document_found)
                })
                .catch((error) => {
                    logger.info(`No users fields already in use: ${error}`)
                    reject(null)
                })
        })
    }

    _isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n)
    }
}
