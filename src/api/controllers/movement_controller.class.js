/**
 * @fileoverview
 * Provides the techubank-mlab movement (CRUD) features.
 *
 */

const moment = require('moment'),
    { movement } = require('../models/movement'),
    {
        logger, mongooseUtils
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class')


module.exports = class MovementController extends BaseController {
    constructor() {
        super()
    }

    static createMovement(req, res) {
        const movement_controller = new MovementController()

        return movement_controller._createMovement(req, res)
    }

    static getMovement(req, res) {
        const movement_controller = new MovementController()

        return movement_controller._getMovement(req, res)
    }

    static findAccountMovements(req, res) {
        const movement_controller = new MovementController()

        return movement_controller._findAccountMovements(req, res)
    }

    static findCardMovements(req, res) {
        const movement_controller = new MovementController()

        return movement_controller._findCardMovements(req, res)
    }

    static updateMovement(req, res) {
        const movement_controller = new MovementController()

        return movement_controller._updateMovement(req, res)
    }

    static createTransferMovements(transfer) {
        const movement_controller = new MovementController()

        return movement_controller._createTransferMovements(transfer)
    }
	
    /**
	 * Function to create a Techubank movement
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    async _createMovement(req, res) {
        logger.info(`Saving new Movement ${req.body.description}: ${req.body.amount}`)

        const new_movement = new movement()


        if (req.body.movement_date && moment(req.body.movement_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.movement_date = moment(req.body.movement_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        if (req.body.value_date && moment(req.body.value_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.value_date = moment(req.body.value_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        Object.assign(new_movement, req.body)
        new_movement.movement_id = new Date().getTime()

        mongooseUtils.save(new_movement)
            .then(() => {
                logger.info('New movement saved successfully')
                res.json(new_movement)
            })
            .catch((error) => {
                logger.info(`Error saving new movement: ${error}`)
                res.statusCode = 400
                res.json({
                    internal_code: 'error-saving-movement',
                    message: 'Error saving new movement. Check input values'
                })
            })

    }


    /**
	 * Function to consult a Techubank movement by it's movement_id
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _getMovement(req, res) {
        logger.info(`Looking up the movement ${req.params.movement_id}`)

        const query = { movement_id: req.params['movement_id'] }

        mongooseUtils.findOne(movement, query)
            .then((document_found) => {
                logger.info('Movement found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the movement: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-movement',
                    message: 'Error looking up the movement. Check input values'
                })
            })
    }

    /**
	 * Function to consult all the movements of a Techubank account
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _findAccountMovements(req, res) {
        logger.info(`Looking up the movements of the account ${req.params.iban}`)

        const query = { iban: req.params['iban'] }
        const order = {movement_date:-1}

        mongooseUtils.findAll(movement, query,order)
            .then((document_found) => {
                logger.info('Movement/s found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the movement/s: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-movement/s',
                    message: 'Error looking up the movement/s. Check input values'
                })
            })
    }

    /**
	 * Function to consult all the movements of a Techubank card
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _findCardMovements(req, res) {
        logger.info(`Looking up the movements of the card ${req.params.pan}`)

        const query = { pan: req.params['pan'] }

        mongooseUtils.findAll(movement, query)
            .then((document_found) => {
                logger.info('Movement/s found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the movement/s: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-movement/s',
                    message: 'Error looking up the movement/s. Check input values'
                })
            })
    }

    /*BUG: hay que decidir qué se puede actualizar de un movimiento. Yo entiendo que solo el estado*/
    /**
	 * Function to update a Techubank movement from it's movement_id
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _updateMovement(req, res) {
        logger.info('Looking up the movement to update')

        const query = { movement_id: req.body.movement_id }

        if (req.body.movement_date && moment(req.body.movement_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.movement_date = moment(req.body.movement_date, 'DD-MM-YYYY')
        } else if (req.body.movement_date) {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        if (req.body.value_date && moment(req.body.value_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.value_date = moment(req.body.value_date, 'DD-MM-YYYY')
        } else if (req.body.value_date) {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        mongooseUtils.findOneAndUpdate(movement, query, req.body)
            .then((document_found) => {
                logger.info('Movement updated successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the movement: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-updating-movement',
                    message: 'Error looking up the movement. Check input values'
                })
            })
    }

    /**
	* Function to create a Techubank movement
	* @param  {Transfer} transfer - Transfer info
	*/
    async _createTransferMovements(transfer) {
        logger.info('Saving movements of the transfer ' + transfer.transfer_id)

        return new Promise((resolve, reject) => {

            let from_movement = new movement()
            let to_movement = new movement()

            const valid_input = {
                movement_id: '',
                transfer_id: transfer.transfer_id,
                amount: 0,
                currency: transfer.currency,
                movement_date: transfer.transfer_date,
                value_date: transfer.transfer_date,
                iban: '',
                status: 'DONE',
                description: transfer.concept
            }


            Object.assign(from_movement, valid_input)
            Object.assign(to_movement, valid_input)

            to_movement.iban = transfer.to_iban
            to_movement.movement_id = `${to_movement.iban}_${transfer.transfer_id}_to_${new Date().getTime()}`
            to_movement.amount = transfer.amount

            from_movement.iban = transfer.from_iban
            from_movement.movement_id = `${from_movement.iban}_${transfer.transfer_id}_from_${new Date().getTime()}`
            from_movement.amount = Number(to_movement.amount) * -1

            mongooseUtils.save(from_movement)
                .then(() => {
                    logger.info('From movement saved successfully')
                    mongooseUtils.save(to_movement)
                        .then(() => {
                            logger.info('To movement saved successfully')
                            resolve(true)
                        })
                        .catch((error) => {
                            logger.info(`Error saving to movement: ${error}`)
                            reject(`Error saving the to movement of the transfer: ${error}`)
                        })
                })
                .catch((error) => {
                    logger.info(`Error saving from movement: ${error}`)
                    reject(`Error saving the from movement of the transfer: ${error}`)
                })
        })
    }
}

