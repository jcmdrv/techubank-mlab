/**
 * @fileoverview
 * Provides the techubank-mlab account (CRUD) features.
 *
 */

const moment = require('moment'),
    { account } = require('../models/account'),
    {
        logger, mongooseUtils
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class')

module.exports = class AccountController extends BaseController {
    constructor() {
        super()
    }

    static createAccount(req, res) {
        const account_controller = new AccountController()

        return account_controller._createAccount(req, res)
    }

    static getAccount(req, res) {
        const account_controller = new AccountController()

        return account_controller._getAccount(req, res)
    }

    static findAccounts(req, res) {
        const account_controller = new AccountController()

        return account_controller._findAccounts(req, res)
    }

    static updateAccount(req, res) {
        const account_controller = new AccountController()

        return account_controller._updateAccount(req, res)
    }

    static getBalance(iban) {
        const account_controller = new AccountController()

        return account_controller._getBalance(iban)
    }

    static updateBalance(iban, balance) {
        const account_controller = new AccountController()

        return account_controller._updateBalance(iban, balance)
    }

    /**
	 * Function to create a Techubank account
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _createAccount(req, res) {
        const new_account = new account()

        logger.info(`Saving new Account ${req.body.title}-${req.body.description}`)

        if (req.body.opening_date && moment(req.body.opening_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.opening_date = moment(req.body.opening_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        Object.assign(new_account, req.body)
        new_account.iban = `ES020182345${new Date().getTime()}`

        mongooseUtils.save(new_account)
            .then(() => {
                logger.info('New account saved successfully')
                return res.json(new_account)
            })
            .catch((error) => {
                logger.info(`Error saving new account: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-saving-account',
                    message: 'Error saving new account. Check input values'
                })
            })
    }


    /**
	 * Function to consult a Techubank account by it's IBAN
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _getAccount(req, res) {
        logger.info(`Looking up the account ${req.params.iban}`)

        const query = { iban: req.params['iban'] }

        mongooseUtils.findOne(account, query)
            .then((document_found) => {
                logger.info('Account found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the account: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-account',
                    message: 'Error looking up the account. Check input values'
                })
            })
    }

    /**
	 * Function to consult all the accounts of a Techubank client (holder or co_holder)
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _findAccounts(req, res) {
        logger.info(`Looking up the accounts of the holder ${req.params.holder_nif}`)

        const query = { $or: [{ holder_nif: req.params['holder_nif'] }, { co_holder_nif: req.params['holder_nif'] }] }

        mongooseUtils.findAll(account, query)
            .then((document_found) => {
                logger.info('Account found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the account: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-account/s',
                    message: 'Error looking up the account. Check input values'
                })
            })
    }

    /**
	 * Function to update on or more data of a Techubank account from it's IBAN
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _updateAccount(req, res) {
        logger.info('Looking up the account to update')

        const query = { iban: req.body.iban }

        if (req.body.opening_date && moment(req.body.opening_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.opening_date = moment(req.body.opening_date, 'DD-MM-YYYY')
        } else if (req.body.opening_date) {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        mongooseUtils.findOneAndUpdate(account, query, req.body)
            .then((document_found) => {
                logger.info('Account updated successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the account: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-updating-account',
                    message: 'Error looking up the account. Check input values'
                })
            })
    }

    /**
	 * Function to consult a Techubank account current balances by it's IBAN
	 * @param  {String} iban - Account iban
	 */
    _getBalance(iban) {
        logger.info(`Looking up the account ${iban}`)

        const query = { iban: `${iban}` }

        return new Promise((resolve, reject) => {
            mongooseUtils.findOne(account, query)
                .then((document_found) => {
                    logger.info('Account balance found successfully')
                    if (!isNaN(document_found.current_balance)) {
                        resolve(document_found.current_balance)
                    }
                })
                .catch((error) => {
                    logger.info(`Error looking up the account balance: ${error}`)
                    reject(`Error looking up the account balance: ${error}`)
                })
        })
    }

    /**
	 * Function to update a Techubank account current balances by it's IBAN
	 * @param  {String} iban - Request info
	 * @param  {Number} balance - Response info
	 */
    _updateBalance(iban, balance) {
        logger.info(`Updating the account ${iban} current balance to ${balance}`)

        const query = { iban: `${iban}` },
            balanceUpdate = { 'current_balance': `${balance}` }

        return new Promise((resolve, reject) => {
            mongooseUtils.findOneAndUpdate(account, query, balanceUpdate)
                .then((document_found) => {
                    logger.info('Account balance updated successfully')
                    if (!isNaN(document_found.current_balance)) {
                        resolve(document_found.current_balance)
                    }
                })
                .catch((error) => {
                    logger.info(`Error updating the account current balance: ${error}`)
                    reject(`Error updating the account current balance: ${error}`)
                })
        })
    }

}
