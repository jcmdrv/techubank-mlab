/**
 * @fileoverview
 * Provides the techubank-mlab card (CRUD) features.
 *
 */

const moment = require('moment'),
    { card } = require('../models/card'),
    {
        logger, mongooseUtils
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class')


module.exports = class CardController extends BaseController {
    constructor() {
        super()
    }

    static createCard(req, res) {
        const card_controller = new CardController()

        return card_controller._createCard(req, res)
    }

    static getCard(req, res) {
        const card_controller = new CardController()

        return card_controller._getCard(req, res)
    }

    static findUserCards(req, res) {
        const card_controller = new CardController()

        return card_controller._findUserCards(req, res)
    }

    static updateCard(req, res) {
        const card_controller = new CardController()

        return card_controller._updateCard(req, res)
    }


    /**
	 * Function to create a Techubank card
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 *
	 */
    _createCard(req, res) {
        const new_card = new card()

        logger.info(`Saving new Card ${req.body.alias}`)

        if (req.body.creation_date && moment(req.body.creation_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.creation_date = moment(req.body.creation_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        if (req.body.expiry_date && moment(req.body.expiry_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.expiry_date = moment(req.body.expiry_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        Object.assign(new_card, req.body)

        mongooseUtils.save(new_card)
            .then(() => {
                logger.info('New card saved successfully')
                res.json(new_card)
            })
            .catch((error) => {
                logger.info(`Error saving new Card: ${error}`)
                res.statusCode = 400
                res.json({
                    internal_code: 'error-saving-card',
                    message: 'Error saving new Card. Check input values'
                })
            })
    }


    /**
	* Function to consult a Techubank card by it's PAN
	* @param  {Object} req - Request info
	* @param  {Object} res - Response info
	*/
    _getCard(req, res) {

        logger.info(`Looking up the card ${req.params.pan}`)

        const query = { pan: req.params['pan'] }

        mongooseUtils.findOne(card, query)
            .then((document_found) => {
                logger.info('Card found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the card: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-card',
                    message: 'Error looking up the card. Check input values'
                })
            })
    }

    /**
	* Function to consult all the cards of a Techubank client
	* @param  {Object} req - Request info
	* @param  {Object} res - Response info
	*/
    _findUserCards(req, res) {

        logger.info(`Looking up the cards of the holder ${req.params.holder_nif}`)

        const query = { holder_nif: req.params['holder_nif'] }

        mongooseUtils.findAll(card, query)
            .then((document_found) => {
                logger.info('Card/s found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the card/s: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-card/s',
                    message: 'Error looking up the card/s. Check input values'
                })
            })
    }


    /**
	* Function to update on or more data of a Techubank card from it's PAN
	* @param  {Object} req - Request info
	* @param  {Object} res - Response info
	*/
    _updateCard(req, res) {

        logger.info('Looking up the card to update')

        const query = { pan: req.body.pan }

        if (req.body.creation_date && moment(req.body.creation_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.creation_date = moment(req.body.creation_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        if (req.body.expiry_date && moment(req.body.expiry_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.expiry_date = moment(req.body.expiry_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        mongooseUtils.findOneAndUpdate(card, query, req.body)
            .then((document_found) => {
                logger.info('Card updated successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the card: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-updating-card',
                    message: 'Error looking up the card. Check input values'
                })
            })
    }

}
