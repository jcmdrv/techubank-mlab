/**
 * @fileoverview
 * Provides the techubank-mlab transfer (CRUD) features.
 *
 */
const moment = require('moment'),
    { transfer } = require('../models/transfer'),
    {
        logger, mongooseUtils
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class')


module.exports = class TransferController extends BaseController {
    constructor() {
        super()
    }

    static createTransfer(req, res) {
        const transfer_controller = new TransferController()

        return transfer_controller._createTransfer(req, res)
    }

    static getTransfer(req, res) {
        const transfer_controller = new TransferController()

        return transfer_controller._getTransfer(req, res)
    }

    static getFavorites(req, res) {
        const transfer_controller = new TransferController()

        return transfer_controller._getFavorites(req, res)
    }

    static findAccountTransfers(req, res) {
        const transfer_controller = new TransferController()

        return transfer_controller._findAccountTransfers(req, res)
    }

    static updateTransfer(req, res) {
        const transfer_controller = new TransferController()

        return transfer_controller._updateTransfer(req, res)
    }


    /**
	 * Function to create a Techubank transfer
 	 * @param  {Object} req - Request info
 	 * @param  {Object} res - Response info
	 */
    _createTransfer(req, res) {
        const new_transfer = new transfer()

        logger.info(`Saving new transfer from ${req.body.from_iban} to ${req.body.to_iban}`)

        if (req.body.transfer_date && moment(req.body.transfer_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.transfer_date = moment(req.body.transfer_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        if (req.body.value_date && moment(req.body.value_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.value_date = moment(req.body.value_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        Object.assign(new_transfer, req.body)

        new_transfer.opening_date = moment(req.body.opening_date, 'DD-MM-YYYY')
        new_transfer.transfer_id = new Date().getTime()

        return mongooseUtils.save(new_transfer)
            .then(() => {
                logger.info('New transfer saved successfully')
                res.json(new_transfer)
            })
            .catch((error) => {
                logger.info(`Error saving new transfer: ${error}`)
                res.statusCode = 400
                res.json({
                    internal_code: 'error-saving-transfer',
                    message: `Error saving new transfer. Check input values: ${error}`
                })
            })
    }


    /**
	 * Function to consult a Techubank transfer by it's transfer_id
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _getTransfer(req, res) {

        logger.info(`Looking up the transfer ${req.params.transfer_id}`)

        const query = { transfer_id: req.params['transfer_id'] }

        return mongooseUtils.findOne(transfer, query)
            .then((document_found) => {
                logger.info('Transfer found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the transfer: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-transfer',
                    message: `Error looking up the transfer. Check input values: ${error}`
                })
            })
    }

    /**
	 * Function to consult the favorite transfers of a Techubank client (nif)
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _getFavorites(req, res) {

        logger.info(`Looking up the favorite transfers of the holder ${req.params.holder_nif}`)

        const query = { $and: [{ from_nif: req.params['holder_nif'] }, { is_favorite: true }] }

        return mongooseUtils.findAll(transfer, query)
            .then((document_found) => {
                logger.info('Transfer/s found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the transfer/s: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-transfer/s',
                    message: `Error looking up the transfer/s. Check input values: ${error}`
                })
            })
    }


    /**
	 * Function to consult all the transfers of a Techubank account
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _findAccountTransfers(req, res) {

        logger.info(`Looking up the transfers of the account ${req.params.iban}`)

        const query = { from_iban: req.params['iban'] }

        return mongooseUtils.findAll(transfer, query)
            .then((document_found) => {
                logger.info('Transfer/s found successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error looking up the transfer/s: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-looking-up-transfer/s',
                    message: `Error looking up the transfer/s. Check input values: ${error}`
                })
            })
    }


    /**
	 * Function to update a Techubank transfer from it's transfer_id
	 * @param  {Object} req - Request info
	 * @param  {Object} res - Response info
	 */
    _updateTransfer(req, res) {

        logger.info('Looking up the transfer to update')

        const query = { transfer_id: req.body.transfer_id }

        if (req.body.transfer_date && moment(req.body.transfer_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.transfer_date = moment(req.body.transfer_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        if (req.body.value_date && moment(req.body.value_date, 'DD-MM-YYYY', true).isValid()) {
            req.body.value_date = moment(req.body.value_date, 'DD-MM-YYYY')
        } else {
            logger.info('Invalid date input')
            res.statusCode = 400
            return res.json({
                internal_code: 'invalid-date',
                message: 'Invalid date input. Check input values'
            })
        }

        return mongooseUtils.findOneAndUpdate(transfer, query, req.body)
            .then((document_found) => {
                logger.info('Transfer updated successfully')
                return res.json(document_found)
            })
            .catch((error) => {
                logger.info(`Error updating the transfer: ${error}`)
                res.statusCode = 400
                return res.json({
                    internal_code: 'error-updating-transfer',
                    message: 'Error updating the transfer. Check input values'
                })
            })
    }

}
