/**
 * @fileoverview
 * Provides the techubank-mlab card model.
 * 
 */

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    CardSchema = new Schema({
        pan: {
            type: String,
            match: [/^[0-9]{16}/, 'Please fill a valid pan'],
            required: true
        },
        title: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid title'],
            required: true
        },
        holder_nif: {
            type: String,
            match: [/\d{8}[-\s]?[a-z A-Z]/, 'Please fill a valid co holder nif'],
            required: true
        },
        linked_account_iban: {
            type: String,
            match: [/([A-Z]{2})([0-9]{22})/, 'Please fill a valid linked account iban']
        },
        type: {
            type: String,
            required: true,
            enum: ['DEBIT', 'CREDIT', 'VIRTUAL', 'RESTAURANT']
        },
        creation_date: {
            type: Date,
            match: [/([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'Please fill a valid creation date'],
            required: true
        },
        expiry_date: {
            type: Date,
            match: [/([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'Please fill a valid expiry date'],
            required: true
        },
        cvv: {
            type: Number,
            match: [/^[0-9]{3,4}$/, 'Please fill a valid CVV'],
            required: true
        },
        status: {
            type: String,
            required: true,
            enum: ['ACTIVE', 'BLOCKED', 'NEGATIVE']
        },
        alias: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid alias']
        },
        atm_limit: {
            type: Number,
            match: [/^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/, 'Please fill a valid amount']
        },
        credit_limit: {
            type: Number,
            match: [/^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/, 'Please fill a valid amount']
        }
    })



CardSchema.index({
    pan: 1
}, {
    unique: true
})

module.exports.card = mongoose.model('card', CardSchema)
module.exports.CardSchema = CardSchema
