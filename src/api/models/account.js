/**
 * @fileoverview
 * Provides the techubank-mlab account model.
 * 
 */

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    {
        AddressSchema
    } = require('./address'),
    AccountSchema = new Schema({
        iban: {
            type: String,
            match: [/([A-Z]{2})([0-9]{22})/, 'Please fill a valid iban'],
            required: true
        },
        title: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid title'],
            required: true
        },
        holder_nif: {
            type: String,
            match: [/\d{8}[-\s]?[a-z A-Z]/, 'Please fill a valid holder nif'],
            required: true
        },
        co_holder_nif: [{
            type: String,
            match: [/\d{8}[-\s]?[a-z A-Z]/, 'Please fill a valid co holder nif']
        }],
        type: {
            type: String,
            required: true,
            enum: ['CHECKING', 'SAVINGS']
        },
        opening_date: {
            type: Date,
            match: [/([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'Please fill a valid opening date'],
            required: true
        },
        status: {
            type: String,
            required: true,
            enum: ['ACTIVE', 'BLOCKED', 'NEGATIVE']
        },
        description: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid description']
        },
        currency: {
            type: String,
            required: true,
            enum: ['EUR', 'DOLLAR']
        },
        available_balance: {
            type: Number,
            match: [/^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/, 'Please fill a valid available balance'],
        },
        current_balance: {
            type: Number,
            match: [/^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/, 'Please fill a valid current balance'],
        },
        correspondence_address: AddressSchema
    })

AccountSchema.index({
    iban: 1
}, {
    unique: true
})

module.exports.account = mongoose.model('account', AccountSchema)
module.exports.AccountSchema = AccountSchema