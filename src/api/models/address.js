const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    AddressSchema = new Schema({
        street: {
            type: String,
            required: true
        },
        zip_code: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        country: {
            type: String,
            required: true
        }
    })

module.exports.AddressSchema = AddressSchema
module.exports.address = mongoose.model('address', AddressSchema)