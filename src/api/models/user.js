/* eslint no-useless-escape: 0 */
/**
 * @fileoverview
 * Provides the techubank-mlab user model.
 *
 */

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    {
        AddressSchema
    } = require('./address'),
    UserSchema = new Schema({
        username: {
            type: String,
            match: [/^[a-z0-9]{1,10}$/, 'Please fill a valid username'],
            required: true
        },
        name: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid name'],
            required: true
        },
        first_name: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid first_name'],
            required: true
        },
        last_name: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid last_name']
        },
        nif: {
            type: String,
            match: [/\d{8}[-\s]?[a-z A-Z]/, 'Please fill a valid nif'],
            required: true
        },
        email: {
            type: String,
            match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
            required: true
        },
        password: {
            type: String,
            match: [/(?=.*\d).{8,20}/, 'Please fill a valid password'],
            required: true
        },
        phone: {
            type: String,
            match: [/^([+]?\d{1,2}[-.\s]?)?(\d{3}[-.\s]?)(\d{2}[-.\s]?)\d{4}$/, 'Please fill a valid telephone number'],
            required: true
        },
        status: {
            type: String,
            required: true,
            enum: ['ACTIVE', 'BLOCKED']
        },
        logged: {
            type: Boolean
        },
        birth_date: {
            type: Date,
            required: true
        },
        register_date: {
            type: Date,
            required: true
        },
        last_login_date: {
            type: Date
        },
        address: AddressSchema,
        scopes: [String]
    })

UserSchema.pre('save', async function (next) {
    const UserController = require('../controllers/user_controller.class')

    let self = this,
        user_controller = new UserController(),
        user = await user_controller._userExists(this)

    if (user) {
        var msg = ''
        if (user.username == self.username) {
            msg += 'Username already used. '
        }
        if (user.nif == self.nif) {
            msg += 'NIF already used. '
        }
        if (user.email == self.email) {
            msg += 'email already used. '
        }
        next(new Error(msg))
    } else
        return next
})

UserSchema.index({
    username: 1
}, {
    unique: true
})

module.exports.UserSchema = UserSchema
module.exports.user = mongoose.model('user', UserSchema)
