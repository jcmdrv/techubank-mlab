/**
 * @fileoverview
 * Provides the techubank-mlab movement model.
 *
 */

const mongoose = require('mongoose'),
    AccountController = require('../controllers/account_controller.class'),
    Schema = mongoose.Schema,
    MovementSchema = new Schema({
        movement_id: {
            type: String,
            match: [/^[a-zA-Z0-9_]{1,100}$/, 'Please fill a movement id'],
            required: true
        },
        transfer_id: {
            type: String,
            match: [/^[a-zA-Z0-9]{1,30}$/, 'Please fill a valid transfer_id']
        },
        amount: {
            type: Number,
            match: [/^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/, 'Please fill a valid amount'],
            required: true
        },
        currency: {
            type: String,
            required: true,
            enum: ['EUR', 'DOLLAR']
        },
        movement_date: {
            type: Date,
            match: [/([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'Please fill a valid movement date'],
            required: true
        },
        value_date: {
            type: Date,
            match: [/([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'Please fill a valid value date'],
            required: true
        },
        iban: {
            type: String,
            match: [/([A-Z]{2})([0-9]{22})/, 'Please fill a valid iban'],
            required: true
        },
        current_balance: {
            type: Number,
            match: [/^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/, 'Please fill a valid current balance']
        },
        pan: {
            type: String,
            match: [/^[0-9]{16}/, 'Please fill a valid pan']
        },
        status: {
            type: String,
            required: true,
            enum: ['REJECTED', 'PENDING', 'DONE']
        },
        description: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid description']
        }
    })

MovementSchema.pre('save', async function(next) {
    let balance = await AccountController.getBalance(this.iban)
    balance = balance + this.amount
    if(balance > 0){
        this.current_balance = balance
        return next
    }
    else
        throw new Error('Not enough balance')
})

MovementSchema.post('save', async function(next) {
    if(this.status == 'DONE'){
        await AccountController.updateBalance(this.iban, this.current_balance)
        return next
    }
})

MovementSchema.index({
    movement_id: 1
}, {
    unique: true
})

module.exports.movement = mongoose.model('movement', MovementSchema)
module.exports.MovementSchema = MovementSchema
