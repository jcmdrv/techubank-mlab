/**
 * @fileoverview
 * Provides the techubank-mlab transfer model.
 * 
 */

const mongoose = require('mongoose'),
    MovementController = require('../controllers/movement_controller.class'),
    Schema = mongoose.Schema,
    TransferSchema = new Schema({
        transfer_id: {
            type: String,
            match: [/^[a-zA-Z0-9]{1,30}$/, 'Please fill a transfer id'],
            required: true
        },
        amount: {
            type: Number,
            match: [/^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/, 'Please fill a valid amount'],
            required: true
        },
        currency: {
            type: String,
            required: true,
            enum: ['EUR', 'DOLLAR']
        },
        transfer_date: {
            type: Date,
            match: [/([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'Please fill a valid transfer date'],
            required: true
        },
        value_date: {
            type: Date,
            match: [/([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'Please fill a valid value date'],
            required: true
        },
        from_iban: {
            type: String,
            match: [/([A-Z]{2})([0-9]{22})/, 'Please fill a valid from iban'],
            required: true
        },
        to_iban: {
            type: String,
            match: [/([A-Z]{2})([0-9]{22})/, 'Please fill a valid to  iban'],
            required: true
        },		
        from_nif: {
            type: String,
            match: [/\d{8}[-\s]?[a-z A-Z]/, 'Please fill a valid nif'],
            required: true
        },
        to_name: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid name'],
            required: true
        },						
        status: {
            type: String,
            required: true,
            enum: ['REJECTED', 'IN PROGRESS', 'DONE']
        },
        concept: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid description']
        },
        when_type: {
            type: String,
            enum: ['ONE-TIME', 'IMMEDIATE', 'SCHEDULED'],
            required: true
        },
        when_info: {
            type: String,
            match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ_ ]{1,50}$/, 'Please fill a valid when info']
        },
        is_favorite: {
            type: Boolean
        }
    }),
    TransfersListSchema = new Schema({
        nif: {
            type: String,
            required: true
        },
        accounts: []
    })

TransferSchema.index({
    transfer_id: 1
}, {
    unique: true
}) 

TransferSchema.pre('save', async function(next) {
		
    if(this.from_iban != this.to_iban){
        await MovementController.createTransferMovements(this)
        return next
    } else {
        throw new Error('Origin and destination account are the same.')
    }
		
})

module.exports.transfer = mongoose.model('transfer', TransferSchema)
module.exports.TransferSchema = TransferSchema
module.exports.TransfersListSchema = TransfersListSchema
module.exports.Transfers_list = mongoose.model('transfers_list', TransfersListSchema)
