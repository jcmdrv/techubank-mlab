/**
 * @fileoverview
 * Provides the techubank-mlab user routes
 *
 */

const express = require('express'),
    user_router = express.Router(),
    {
        environment, enable_cors, check_security_headers_admin_role
    } = require('techubank-commons'),
    UserController = require('../controllers/user_controller.class')


// Add OAuth Security
user_router.use(check_security_headers_admin_role) 

if (environment.value.enableCors){
    user_router.use(enable_cors)
}

user_router.post('/', UserController.createUser)
user_router.put('/', UserController.updateUser)
user_router.get('/:username/info', UserController.getUser)
user_router.post('/find', UserController.findUsers)
user_router.put('/changePassword', UserController.changePassword)
user_router.put('/resetPassword', UserController.resetPassword)

module.exports = user_router
