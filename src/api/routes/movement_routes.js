/**
 * @fileoverview
 * Provides the techubank-mlab movement routes
 *
 */

const express = require('express'),
    movement_router = express.Router(),
    {
        environment, enable_cors, check_security_headers_admin_role
    } = require('techubank-commons'),
    MovementController = require('../controllers/movement_controller.class')

// Add OAuth Security
movement_router.use(check_security_headers_admin_role)

if (environment.value.enableCors) {
    movement_router.use(enable_cors)
}

movement_router.post('/', MovementController.createMovement)
movement_router.put('/', MovementController.updateMovement)
movement_router.get('/:movement_id', MovementController.getMovement)
movement_router.get('/account/:iban', MovementController.findAccountMovements)
movement_router.get('/card/:pan', MovementController.findCardMovements)

module.exports = movement_router
