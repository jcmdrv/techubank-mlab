/**
 * @fileoverview
 * Provides the techubank-mlab card routes
 *
 */

const express = require('express'),
    card_router = express.Router(),
    {
        environment, enable_cors, check_security_headers_admin_role
    } = require('techubank-commons'),
    CardController = require('../controllers/card_controller.class')


// Add OAuth Security
card_router.use(check_security_headers_admin_role)

if (environment.value.enableCors){
    card_router.use(enable_cors)
}

card_router.post('/', CardController.createCard)
card_router.put('/', CardController.updateCard)
card_router.get('/holder/:holder_nif', CardController.findUserCards)
card_router.get('/pan/:pan', CardController.getCard)

module.exports = card_router
