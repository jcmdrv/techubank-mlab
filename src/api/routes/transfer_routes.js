/**
 * @fileoverview
 * Provides the techubank-mlab transfer routes
 * 
 */

const express = require('express'),
    transfer_router = express.Router(),
    {
        environment, enable_cors, check_security_headers_admin_role
    } = require('techubank-commons'),
    TransferController = require('../controllers/transfer_controller.class')


// Add OAuth Security
transfer_router.use(check_security_headers_admin_role) 

if (environment.value.enableCors){
    transfer_router.use(enable_cors)
}

transfer_router.post('/', TransferController.createTransfer)
transfer_router.put('/', TransferController.updateTransfer)
transfer_router.get('/:transfer_id', TransferController.getTransfer)
transfer_router.get('/holder/:holder_nif', TransferController.getFavorites)
transfer_router.get('/account/:iban', TransferController.findAccountTransfers)

module.exports = transfer_router