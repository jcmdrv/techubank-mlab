/**
 * @fileoverview
 * Provides the techubank-mlab account routes
 * 
 */

const express = require('express'),
    account_router = express.Router(),
    {
        environment, enable_cors, check_security_headers_admin_role
    } = require('techubank-commons'),
    AccountController = require('../controllers/account_controller.class')

// Add OAuth Security
account_router.use(check_security_headers_admin_role)

if (environment.value.enableCors){
    account_router.use(enable_cors)
}

account_router.post('/', AccountController.createAccount)
account_router.put('/', AccountController.updateAccount)
account_router.get('/holder/:holder_nif', AccountController.findAccounts)
account_router.get('/iban/:iban', AccountController.getAccount)

module.exports = account_router