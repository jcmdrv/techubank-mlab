# Introduction
TechU Bank rest MLAB server allows the *Techubank Api* to interact with the MongoDB.

Entities managed by the API:
- User
- Accounts
- Transfers
- Movements
- Cards

## API Rest Documentation

The API documentation is written following the Open API v2 specification in a Swagger YAML file.

You can find all the specification versions in ws-doc folder. It's automatically rendered using swagger-ui library.56936234F

In localhost you can check the documentation and try any endpoint:

1) Start the nodejs server
```
$ npm install
$ npm run start

```
2) Access the [ws-doc link](http://localhost:5000/ws-doc)
3) You will be able to test the endpoints setting the apki-key variable with the value "test"


# Quick Start

One way to use the API of MLab is using the [MLab Postman collection](https://www.getpostman.com/collections/77e43c22b277005530c6)

Create a *.env* file on the base path with MongoDB credentials
```
MLAB_HOST=<mongo-host>
MLAB_DATABASENAME=<mongo-databasename>
MLAB_USERNAME=<username>
MLAB_PASSWORD=<pwd>
```
The TechUBank Oauth Server must be running. [Follow these instructions](https://bitbucket.org/jcmdrv/techubank-auth-server/src/master/README.md)

Install dependencies and launch the Mlab server
```
$ npm install

$ npm run start
```

```
$ npm run start

> techubank-mlab@1.0.33 start /home/alumno/project/techubank-mlab
> nodemon src/index.js

[nodemon] 1.18.7
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: *.*
[nodemon] starting `node src/index.js`
info: ************************
info: Logger initialized
info: ************************
info: Properties from environment.local loaded
info: ***** techubank-mlab@1.0.33 *************
info: App listening HTTP on port 5000
info: App listening HTTPS on port 5443
```

# Development

# Requirements
This project needs:
- Nodejs 8.4+
- Docker 17+ if you want to publish a new docker image or run the latest TechU Bank docker image
- The TechUBank Oauth Server running. [Follow these instructions](https://bitbucket.org/jcmdrv/techubank-auth-server/src/master/README.md)
- Access to a MongoDB instance with a valid *users* collection.


## Folders

- bin: helpers
	- pre-commit: execute linter and unit tests before a commit
	- swagger-codegen.sh: parse swagger.vX.yaml and generates an api-client and a scaffold for the server side.
- src: The API logic
- ws-doc: Here you can find the swager.v1.yml with API's documentation
- test: unit tests folder


## Compilation
Stay on the base path where package.json is located.

```
$ npm install


If you want to update packages

$ npm update

```


## Unit tests

1) Stay on the base path where package.json is located.

```
techu@techu> npm run test

```

2) If you want to execute unit tests on each change, execute the watcher command.
```
techu@techu> npm run watcher

```

3) To get coverage metrics run Instanbul
```
techu@techu> npm run coverage

```


# Static code analysis

## Linter
Linter is a light weight static code analysis recomended to be execute in every push to a branch, and may block the push.
```
techu@techu> npm run lint

```
## Sonarqube
We have prepared a container with the software enough for run a static code analysis, execute unit tests, measure unit test coverage and upload the results to Sonarqube (free account).

You must have a Sonarqube API Key, configure properly the sonar-scanner.properties and run the command below:
```
$ docker run -it --env SONAR_API_KEY=<SONAR_USER_API_KEY> --mount type=bind,source="$(pwd)",target=/root/project -v "$(pwd)"/sonar-scanner.properties:/root/sonar-scanner/conf/sonar-scanner.properties davidrova/sonar-scanner:latest
```

To see the result [click on this link](https://sonarcloud.io/projects)



## Hooks
We encourage to enable the pre-commit hook. It will avoid commiting a change if linter or unit tests fail.

```
$ cp bin/pre-commit .git/hooks/pre-commit

```

## Continuous integration
The project is managed in bitbucket.org and we have automated an ease pipeline with the following step rules:

- A commint into *develop* branch triggers:
	- build
	- lint
	- unit tests
	- sonar
	- Increases the *patch version*
	- Publish the artifact to npmrc registry
	- Publish a new docker image into Docker Hub
- A commit into *master* branch triggers:
	- build
	- lint
	- unit tests
	- sonar
	- Increases the *minor version*
	- Publish the artifact to npmrc registry
	- Publish a new docker image into Docker Hub


NOTE: It's neccessary to generate a new key pair. [Follow these instructions](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html?_ga=2.62235695.2086604014.1539165652-1490061868.1526909294)


# TechU Bank MLab Server Docker image

# Start TechU Bank MLab Server docker image
```
docker run -it -p 4000:4000 -p 4443:4443 -e PORT=4000 -e PORT_SSL=4443 -e MLAB_HOST=<host>:<port> -e MLAB_DATABASENAME=<databasename> -e MLAB_USERNAME=<mlab-username> -e MLAB_PASSWORD=<mlab-password> -e ENV=docker ${DOCKER_HUB_USERNAME}/techubank-mlab-server
```

# Stop TechU Bank MLab Server docker image
```
docker stop techubank__mlab__server__testing
```

# Remove TechU Bank MLab Server docker image
```
docker rm techubank__mlab__server__testing
```

# Building and push the docker image

```
docker build -t ${DOCKER_HUB_USERNAME}/techubank-mlab-server .
docker login --username=${DOCKER_HUB_USERNAME} --password=${DOCKER_HUB_PASSWORD}
docker push ${DOCKER_HUB_USERNAME}/techubank-mlab-server
```

# Environment variables
This API is securized via OAuth2 method; by default it is setup our own OAuth 2 Server.

techubank-mlab is the component which interact with MongoDB; For the connection with MongoDB you have to facilitate a host, database name, username and password (by default ds133353.mlab.com:33353, techubank-jcmdrv, techu, techu1234). And for the interaction with the Oauth server a username and password of an admin user (by default techubank / techubank1234).

Create a _.env_ file on the base path with the following variables

```
MLAB_HOST=<host>
MLAB_DATABASENAME=<databasename>
MLAB_USERNAME=<username>
MLAB_PASSWORD=<password>
API_USERNAME=<username-with-admin-scope>
API_PASSWORD=<password>
```

# Setup https

Create an self-signed SSL certificate

* Write down the Common Name (CN) for your SSL Certificate. The CN is the fully qualified name for the system that uses the certificate. If you are using Dynamic DNS, your CN should have a wild-card, for example: *.api.com. Otherwise, use the hostname or IP address set in your Gateway Cluster (for example. 192.16.183.131 or dp1.acme.com).

* Run the following OpenSSL command to generate your private key and public certificate. Answer the questions and enter the Common Name when prompted.

```
$ openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem


Country Name (2 letter code) [AU]:localhost
string is too long, it needs to be no more than 2 bytes long
Country Name (2 letter code) [AU]:ES
State or Province Name (full name) [Some-State]:MADRID
Locality Name (eg, city) []:MADRID
Organization Name (eg, company) [Internet Widgits Pty Ltd]:techubank
Organizational Unit Name (eg, section) []:techubank
Common Name (e.g. server FQDN or YOUR name) []:techubank
Email Address []:techubank@techu.com

```
* key.pem is the private key
* certificate.pem is the self-signed certificate

* Copy the pem files on the basepath

```
alumno@BootcampVM:~/project/techubank-mlab$ ls -lrt
total 496
-rw-rw-r--   1 alumno alumno    454 nov 28 15:24 Dockerfile
-rw-rw-r--   1 alumno alumno   1436 nov 28 15:24 certificate.pem
drwxrwxr-x   2 alumno alumno   4096 nov 28 15:24 bin
-rw-rw-r--   1 alumno alumno    667 nov 28 15:24 sonar-scanner.properties
-rw-rw-r--   1 alumno alumno   1704 nov 28 15:24 key.pem
drwxrwxr-x   3 alumno alumno   4096 nov 28 15:24 test
drwxrwxr-x   2 alumno alumno   4096 nov 28 15:24 ws-doc
-rw-rw-r--   1 alumno alumno      0 nov 28 15:27 error.log
drwxrwxr-x   3 alumno alumno   4096 nov 29 21:50 src
-rw-rw-r--   1 alumno alumno    697 nov 29 21:50 sonar-project.properties
drwxrwxr-x   3 alumno alumno   4096 nov 30 12:22 report-coverage
-rw-rw-r--   1 alumno alumno   2069 dic  1 19:20 bitbucket-pipelines.yml
-rw-rw-r--   1 alumno alumno   1525 dic  1 19:20 package.json
drwxrwxr-x 450 alumno alumno  16384 dic  1 19:20 node_modules
-rw-rw-r--   1 alumno alumno 409463 dic  1 19:20 package-lock.json
-rw-rw-r--   1 alumno alumno  20372 dic  1 19:20 techubank.log
-rw-rw-r--   1 alumno alumno   7166 dic  1 19:30 README.md
```

References:
* https://www.sitepoint.com/how-to-use-ssltls-with-node-js/
* https://www.ibm.com/support/knowledgecenter/en/SSWHYP_4.0.0/com.ibm.apimgmt.cmc.doc/task_apionprem_gernerate_self_signed_openSSL.html
