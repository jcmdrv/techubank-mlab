/* global describe it */

/**
 * @fileoverview
 * Provides the techubank-mlab movement test.
 *
 */

const expect = require('chai').expect,
    {movement} = require('../../src/api/models/movement')

describe('Check a basic movement payload with all its attributes filled (required and optional)', function() {
    it('Should be invalid if id, amount, currency, movement_date, value_date, iban, status or description are empty', function(done) {
        const valid_input = {
                'movement_id': '12345678',
                'amount': '34',
                'currency': 'EUR',
                'movement_date': new Date(),
                'value_date': new Date(),
                'iban': 'ES0201820123456789012345',
                'pan': '0123456789012345',
                'status': 'REJECTED',
                'description': 'Mercadona'
            },
            my_movement = new movement()

        Object.assign(my_movement, valid_input)

        my_movement.validate(function(err) {
            expect(err).to.not.exist
            done()
        })
    })
})
