/* global describe it */

/**
 * @fileoverview
 * Provides the techubank-mlab transfer test.
 *
 */

const expect = require('chai').expect,
    {transfer} = require('../../src/api/models/transfer')

describe('Check a basic transfer payload with all its attributes filled (required and optional)', function() {
    it('Should be invalid if iban, title, holder_nif, type, opening_date, status or currency are empty', function(done) {
        const valid_input = {
                'transfer_id': 'sifjbndijvdf',
                'amount':'500.00',
                'currency': 'EUR',
                'transfer_date': new Date(),
                'value_date': new Date(),
                'from_iban': 'ES0201820123456789012345',
                'to_iban': 'ES0201820123456789032345',
                'to_name': 'Menganito',
                'from_nif': '98765432S',
                'status': 'IN PROGRESS',
                'concept': 'VARIOS',
                'when_type': 'ONE-TIME',
                'when_info': 'INFO'
            },
            my_transfer = new transfer()

        Object.assign(my_transfer, valid_input)

        my_transfer.validate(function(err) {
            expect(err).to.not.exist
            done()
        })
    })
})
