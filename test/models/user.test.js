/* global describe it */

/**
 * @fileoverview
 * Provides the techubank-mlab user test.
 *
 */

const expect = require('chai').expect,
    {
        user
    } = require('../../src/api/models/user'),
    {
        address
    } = require('../../src/api/models/address')

describe('Check a basic user payload with all its attributes filled (required and optional)', function () {
    it('Should be invalid if username, name, first_name, nif, email, password, phone, user_status, or register_date are empty', function (done) {
        const valid_input = {
                'username': 'mynickname',
                'name': 'Pepito',
                'first_name': 'Name',
                'last_name': 'Surname',
                'nif': '12345678J',
                'birth_date': new Date(),
                'phone': '+34666666666',
                'email': 'user@bbva.com',
                'password': 'secret45!',
                'status': 'ACTIVE',
                'logged': true,
                'register_date': new Date(),
                'user_status': 'ACTIVE',
                'address': {
                    'street': 'string',
                    'zip_code': 'string',
                    'city': 'string',
                    'country': 'string'
                }
            },
            my_user = new user()

        my_user.address = new address()

        Object.assign(my_user, valid_input)
        Object.assign(my_user.address, valid_input.address)

        my_user.validate(function (err) {
            expect(err).to.not.exist
            done()
        })
    })
})