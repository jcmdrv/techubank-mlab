/* global describe it */

/**
 * @fileoverview
 * Provides the techubank-mlab card test.
 *
 */

const expect = require('chai').expect,
    {card} = require('../../src/api/models/card')

describe('Check a basic card payload with all its attributes filled (required and optional)', function() {
    it('Should be invalid if pan, title, holder_nif, type, creation_date, expiry_date, cvv or status are empty', function(done) {
        const valid_input = {
                'pan': '0123456789012345',
                'title': 'Fulanito',
                'holder_nif': '12345678A',
                'linked_account_iban': 'ES0201820123456789012345',
                'type': 'DEBIT',
                'creation_date': new Date(),
                'expiry_date': new Date(),
                'cvv': '123',
                'status': 'ACTIVE',
                'alias': 'TARJETA DE PRUEBA',
                'atm_limit': '900'
            },
            my_card = new card()

        Object.assign(my_card, valid_input)

        my_card.validate(function(err) {
            expect(err).to.not.exist
            done()
        })
    })
})
