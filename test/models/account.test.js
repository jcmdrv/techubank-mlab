/* global describe it */

/**
 * @fileoverview
 * Provides the techubank-mlab account test.
 *
 */

const expect = require('chai').expect,
    {
        account
    } = require('../../src/api/models/account')

describe('Check a basic account payload with all its attributes filled (required and optional)', function () {
    it('Should be invalid if iban, title, holder_nif, type, opening_date, status or currency are empty', function (done) {
        const valid_input = {
                'iban': 'ES0201820123456789012345',
                'title': 'Menganito',
                'holder_nif': '98765432S',
                'co_holder_nif': '98765432S',
                'type': 'CHECKING',
                'opening_date': new Date(),
                'status': 'ACTIVE',
                'description': 'Cuenta corriente',
                'currency': 'EUR',
                'available_balance': '-500',
                'current_balance': '500',
                'correspondence_address': {
                    'street': 'Calle Batanes 3',
                    'zip_code': '28760',
                    'city': 'Tres Cantos',
                    'country': 'Spain'
                }
            },
            my_account = new account()

        Object.assign(my_account, valid_input)

        my_account.validate(function (err) {
            expect(err).to.not.exist
            done()
        })
    })
})
